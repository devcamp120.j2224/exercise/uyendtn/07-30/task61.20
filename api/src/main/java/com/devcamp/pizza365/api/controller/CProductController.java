package com.devcamp.pizza365.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.api.model.CMenu;
import com.devcamp.pizza365.api.repository.IProductsRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CProductController {
    @Autowired
    IProductsRepository iProductsRepository;

    @GetMapping("/menu")
    public ResponseEntity<List<CMenu>> getProductsList() {
        List<CMenu> productList = new ArrayList<>();

        try {
            iProductsRepository.findAll().forEach(productList::add);
            return new ResponseEntity<>(productList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
