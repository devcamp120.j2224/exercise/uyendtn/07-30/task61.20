package com.devcamp.pizza365.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.api.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer,Long> {

    CCustomer findById(int customerId);

    
}
