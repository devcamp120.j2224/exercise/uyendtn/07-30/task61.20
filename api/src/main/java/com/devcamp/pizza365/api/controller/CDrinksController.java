package com.devcamp.pizza365.api.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.api.model.CDrinks;
import com.devcamp.pizza365.api.repository.IDrinksRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDrinksController {
    @Autowired
    IDrinksRepository iDrinks;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrinks>> getDrinks() {
        try {
            List<CDrinks> listDrinks = new ArrayList<CDrinks>();
            iDrinks.findAll().forEach(listDrinks::add);
            return new ResponseEntity<List<CDrinks>>(listDrinks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
