package com.devcamp.pizza365.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.api.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

    CVoucher findById(String maVoucher);
}
