package com.devcamp.pizza365.api.model;

import javax.persistence.*;

@Entity
@Table(name="drinks")
public class CDrinks {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name="ma_nuoc_uong")
    private String maNuocUong;

    @Column(name="ten_nuoc_uong")
    private String tenNuocUong;

    @Column(name="don_gia")
    private double donGia;

    @Column(name="ngay_Tao")
    private long ngayTao;

    @Column(name="ngay_cap_nhat")
    private long ngayCapNhat;

    public CDrinks() {
    }
    public CDrinks(int id, String maNuocUong, String tenNuocUong, double donGia, long ngayTao, long ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getMaNuocUong() {
        return maNuocUong;
    }
    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }
    public String getTenNuocUong() {
        return tenNuocUong;
    }
    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }
    public double getDonGia() {
        return donGia;
    }
    public void setDonGia(double donGia) {
        this.donGia = donGia;
    }
    public long getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }
    public long getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    
}
