package com.devcamp.pizza365.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.api.model.CCustomer;
import com.devcamp.pizza365.api.model.COrder;
import com.devcamp.pizza365.api.repository.ICustomerRepository;
import com.devcamp.pizza365.api.repository.IOrderRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerOrderController {
    @Autowired
    ICustomerRepository iCustomerRepository;
    IOrderRepository iOrderRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getCustomersList(){
        try {
            List<CCustomer> customerList = new ArrayList<>();
           iCustomerRepository.findAll().forEach(customerList::add);
            return new ResponseEntity<>(customerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders")
    public ResponseEntity<Set<COrder>> getOrdersListByCustomerId(@RequestParam(name = "customerId",required = true) int customerId) {
        try {
            CCustomer inputCustomer =  iCustomerRepository.findById(customerId);
            if(inputCustomer!=null){
                return new ResponseEntity<>(inputCustomer.getOrders(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
