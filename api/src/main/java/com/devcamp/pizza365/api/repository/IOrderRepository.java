package com.devcamp.pizza365.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.api.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder,Long> {
    
}
