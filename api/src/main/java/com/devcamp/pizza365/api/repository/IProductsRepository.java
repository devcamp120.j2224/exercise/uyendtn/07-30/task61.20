package com.devcamp.pizza365.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.api.model.CMenu;

public interface IProductsRepository extends JpaRepository<CMenu,Long> {
    
}
