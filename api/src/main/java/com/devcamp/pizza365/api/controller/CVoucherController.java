package com.devcamp.pizza365.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.api.model.CVoucher;
import com.devcamp.pizza365.api.repository.IVoucherRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CVoucherController {
    @Autowired
    IVoucherRepository pIVoucherRepository;
    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        try {
            List<CVoucher> pCVouchers = new ArrayList<CVoucher>();
            pIVoucherRepository.findAll().forEach(pCVouchers::add);
            return new ResponseEntity<List<CVoucher>>(pCVouchers, HttpStatus.OK);
        } catch (Exception e) {
           return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // @GetMapping("/vouchers")
    // public ResponseEntity<CVoucher> getOrdersListByCustomerId(@RequestParam(name = "maVoucher",required = true) String maVoucher) {
    //     try {
    //         CVoucher inputVoucher =  pIVoucherRepository.findById(maVoucher);
    //         if(inputVoucher!=null){
    //             return new ResponseEntity<>(inputVoucher, HttpStatus.OK);
    //         } else{
    //             return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
    //         }
    //     } catch (Exception e) {
    //         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    //     }
    // }
}
