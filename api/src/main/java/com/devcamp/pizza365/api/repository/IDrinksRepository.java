package com.devcamp.pizza365.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.api.model.CDrinks;

public interface IDrinksRepository extends JpaRepository<CDrinks,Long> {
    
}
